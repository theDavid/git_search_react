import React, { Component } from 'react';

class RepoItem extends Component {
  getIssues(id){
    this.props.getIssues(id)
  }

  render() {
    return (
      <tr>
        <td><a href=" #" onClick={this.getIssues.bind(this, this.props.repo.full_name)}>{this.props.repo.html_url}</a></td>
        <td className="text-center">{this.props.repo.description}</td>
        <td className="text-center">{this.props.repo.forks_count}</td>
        <td className="text-center">{this.props.repo.stargazers_count}</td>
        <td className="text-center">{this.props.repo.open_issues_count}</td>
      </tr>
    );
  }
}

export default RepoItem;
