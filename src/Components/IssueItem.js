import React, { Component } from 'react';

class IssueItem extends Component {
  render() {
    return (
      <tr>
        <td>{this.props.issue.html_url}</td>
        <td className="text-center">{this.props.issue.title}</td>
        <td className="text-center">{this.props.issue.user.login}</td>
        <td className="text-center">{this.props.issue.comments}</td>
      </tr>
    );
  }
}

export default IssueItem;
