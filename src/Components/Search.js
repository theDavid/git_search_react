import React, { Component } from 'react';
import { debounce } from 'throttle-debounce';
import loading from '../res/loading.gif';

class Search extends Component {
  constructor() {
    super();
    this.callAjax = debounce(500, this.callAjax); // waiting 500 ms before the call
  }
  
  inputChange(e) {
    this.callAjax(e.target.value);
  }

  callAjax(value) {
    this.props.getrepos(value)
  }

  render() {
    return (
      <div className="row search-row">
        <form className="search-repos col-6">
          <input type="text" onKeyUp={this.inputChange.bind(this)}/>
        </form>

        { this.props.loading ?
          <div className="text-center">
            <img src={loading} alt="loading" width="50" height="50"/>
          </div>
        : null }
        
      </div>
    );
  }
}

export default Search;
