import React, { PureComponent } from 'react';
import RepoItem from './RepoItem';

class Repos extends PureComponent {
  getIssues(id){
    this.props.getissues(id)
  }

  render() {
    let repoItems;
    if(this.props.repos){
      repoItems = this.props.repos.map(repo => {
        return (
          <RepoItem getIssues={this.getIssues.bind(this)} key={repo.id} repo={repo} />
        )
      })
    }

    return (
      <div className="container">
        <div className="row">
          <table className="table table-striped table-dark col">
            <thead>
              <tr>
                <th scope="col">Github URL</th>
                <th scope="col">Description</th>
                <th scope="col">Fork</th>
                <th scope="col">Stargazers</th>
                <th scope="col">Issues</th>
              </tr>
            </thead>
            <tbody>
              {repoItems}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Repos;
