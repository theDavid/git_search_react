import React, { PureComponent } from 'react';
import IssueItem from './IssueItem';

class Issues extends PureComponent {
  goToRepos(){
    this.props.gotorepos()
  }

  render() {
    let issueItems;
    if(this.props.issues){
      issueItems = this.props.issues.map(issue => {
        return (
          <IssueItem key={issue.id} issue={issue} />
        )
      })
    }

    return (
      <div className="container">
        <a href=" #" onClick={this.goToRepos.bind(this)}>Back to repos</a>
        <div className="row">
          <table className="table table-striped table-dark col">
          <thead>
              <tr>
                <th scope="col">Issue URL</th>
                <th scope="col">Title</th>
                <th scope="col">User</th>
                <th scope="col">Comments</th>
              </tr>
            </thead>
            <tbody>
              {issueItems}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Issues;
