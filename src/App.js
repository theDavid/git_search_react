import React, { Component } from 'react';
import $ from 'jquery';
import './App.css';
import Search from './Components/Search';
import Repos from './Components/Repos';
import Issues from './Components/Issues'

class App extends Component {
  constructor(){
    super();
    this.state = {
      keyword: 'react', // default keyword to search with
      repos: [], // container for the repos
      repoMode: true,
      loading: false,
    }
  }

  getRepos( value ){
    let keyword = value || this.state.keyword; // defaulting to the keyword of the current state
    this.setState((prevState, props) => {
      return {loading: true};
    });
    $.ajax({
      url: 'https://api.github.com/search/repositories?q=' + keyword,
      dataType: 'json',
      cache: false,
      success: function(data){
        this.setState({repos: data.items, loading: false}, function(){
      });
      }.bind(this),
      error: function(xhr, status, err){
        console.log(err);
      }
    })
  }

  getIssues( repo ){
    this.setState((prevState, props) => {
      return {loading: true};
    });
    $.ajax({
      url: 'https://api.github.com/search/issues?q=' + repo,
      dataType: 'json',
      cache: false,
      success: function(data){
        this.setState({issues: data.items}, function(){
          this.setState((prevState, props) => {
            return {repoMode: false, loading: false};
          });
      });
      }.bind(this),
      error: function(xhr, status, err){
        console.log(err);
      }
    })
  }

  goToRepos(){
    this.setState((prevState, props) => {
      return {repoMode: true};
    });
  }

  componentDidMount(){
    this.getRepos(); // searching with default keyword on component mount
  }

  render() {
    return (
      <div className="App">
        <div className="search-results">
          <div className="container">
            {this.state.repoMode && <Search getrepos={this.getRepos.bind(this)} loading={this.state.loading}/>}
            {this.state.repoMode && <Repos repos={this.state.repos} getissues={this.getIssues.bind(this)}/>}
            {!this.state.repoMode && <Issues issues={this.state.issues} gotorepos={this.goToRepos.bind(this)}/>}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
